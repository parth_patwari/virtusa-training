CREATE TABLE IF NOT EXISTS student (
  id INTEGER NOT NULL AUTO_INCREMENT,
  username VARCHAR(200) NOT NULL,
  first_name VARCHAR(120) NOT NULL,
  last_name VARCHAR(120) NOT NULL,
  email VARCHAR(200) NOT NULL,

  PRIMARY KEY(id)
);
CREATE TABLE IF NOT EXISTS Subjects(
 subject VARCHAR(100) NOT NULL
);

INSERT INTO Subjects (subject) values ('SCIENCE');
INSERT INTO Subjects (subject) values ('MATHEMATICS');
INSERT INTO Subjects (subject) values ('HISTORY');


CREATE TABLE IF NOT EXISTs teacher (
  t_id INTEGER NOT NULL AUTO_INCREMENT,
  username VARCHAR(200) NOT NULL,
  first_name VARCHAR(120) NOT NULL,
  last_name VARCHAR(120) NOT NULL,
  email VARCHAR(200) NOT NULL,
  subject VARCHAR(100) NOT NULL,
  password VARCHAR(150) NOT NULL,

  PRIMARY KEY(t_id),
  FOREIGN KEY (subject) REFERENCES Subjects(subject)
);


CREATE TABLE IF NOT EXISTS StudentGrades(
grades_id INTEGER NOT NULL AUTO_INCREMENT,
   s_id INTEGER NOT NULL,
   subject VARCHAR(100) NOT NULL,
   score INTEGER DEFAULT 0,

   FOREIGN KEY (s_id) REFERENCES student(id) ON DELETE CASCADE,
   FOREIGN KEY (subject) REFERENCES Subjects(subject)

);