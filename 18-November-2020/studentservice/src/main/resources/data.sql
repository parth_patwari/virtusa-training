INSERT INTO student (username, first_name, last_name, email) VALUES ('MJohnson','Mark', 'Johnson', 'mj@virtusa.com');
INSERT INTO student (username, first_name, last_name, email) VALUES ('JDoe','John', 'Doe', 'jd@virtusa.com');
INSERT INTO student (username, first_name, last_name, email) VALUES('MJosh','Micheal', 'Josh', 'mjosh@virtusa.com');
INSERT INTO student (username, first_name, last_name, email) VALUES ('JKen','Jonathan', 'ken', 'jken@virtusa.com');
INSERT INTO student (username, first_name, last_name, email) VALUES ('MLee','Mike', 'Lee', 'mlee@virtusa.com');

INSERT INTO teacher (username, first_name, last_name, email, password, subject) VALUES ('JCooper', 'Jake',
                                                               'Cooper', 'jcp@virtusa.com', 'J123', 'HISTORY');
INSERT INTO teacher (username, first_name, last_name, email, password, subject) VALUES ('BJones', 'Betty',
                                                               'Jones', 'bjn@virtusa.com', 'B123', 'MATHEMATICS');
INSERT INTO teacher (username, first_name, last_name, email, password, subject) VALUES('JCooper', 'Erik',
                                                               'Clark', 'eclk@virtusa.com', 'E123', 'SCIENCE');



INSERT INTO StudentGrades (s_id, subject, score) VALUES (1, 'SCIENCE', 100);
INSERT INTO StudentGrades (s_id, subject, score) VALUES (1, 'MATHEMATICS', 100);
INSERT INTO StudentGrades (s_id, subject, score) VALUES (1, 'HISTORY', 80);

INSERT INTO StudentGrades (s_id, subject, score) VALUES (2, 'SCIENCE', 87);
INSERT INTO StudentGrades (s_id, subject, score) VALUES (2, 'MATHEMATICS', 95);
INSERT INTO StudentGrades (s_id, subject, score) VALUES (2, 'HISTORY', 78);

INSERT INTO StudentGrades (s_id, subject, score) VALUES (3, 'SCIENCE', 80);
INSERT INTO StudentGrades (s_id, subject, score) VALUES (3, 'MATHEMATICS', 84);
INSERT INTO StudentGrades (s_id, subject, score) VALUES (3, 'HISTORY', 90);