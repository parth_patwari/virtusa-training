package com.springboot.training.studentservice.controllers;

import com.springboot.training.studentservice.model.Student;
import com.springboot.training.studentservice.service.StudentService;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping
    public List<Student> getstudent(){
       return studentService.getAllStudents();
    }
    @GetMapping("/{id}")
    public String getstudentbyid(@PathVariable("id") int  id){
        return studentService.getStudentByID(id).toString();
    }

    @GetMapping("/fname/{fname}")
    public List<Student> getstudentbyfname(@PathVariable("fname") String fname){
        return studentService.getStudentsbyFirstName(fname);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<List<Student>> getstudentbyname(@PathVariable("name") String name){

        if(studentService.getStudentsbyName(name).isEmpty()){
            return new ResponseEntity<List<Student>>(HttpStatus.NO_CONTENT);
        }
        else{
            return ResponseEntity.status(HttpStatus.OK).body(studentService.getStudentsbyName(name));
        }
    }

    @PostMapping("/add")
    public List<Student> addStudent(@RequestBody Student student){
        System.out.println(student.toString());
        int id = studentService.addStudent(student);
        return studentService.getAllStudents();
    }

    @DeleteMapping("/delete/{id}")
    public List<Student> deleteStudentbyID(@PathVariable("id") int id){
        studentService.deleteStudent(id);
        return studentService.getAllStudents();
    }

    @PutMapping("/update/{username}")
    public List<Student> UpdateStudent(@PathVariable("username") String username, @RequestBody Student student){
        studentService.updateStudent(username, student);
        return studentService.getAllStudents();
    }
    @GetMapping("/new")
    public String newstudent(){
        return "In Process.....";
    }


}

