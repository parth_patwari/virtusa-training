package com.springboot.training.studentservice.service;

import com.springboot.training.studentservice.model.SUBJECT;
import com.springboot.training.studentservice.model.Teacher;
import com.springboot.training.studentservice.model.entity.TeacherEntity;
import com.springboot.training.studentservice.repository.teacher.TeacherDbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {

//    @Autowired
//    TeacherRepository teacherRepository;

    @Autowired
    TeacherDbRepository teacherDbRepository;

    @Override
    public void addTeacher(Teacher teacher) {
        TeacherEntity te = new TeacherEntity(teacher.getUsername(), teacher.getFirstName(),
                teacher.getLastName(), teacher.getEmail(), teacher.getPassword()
        ,teacher.getSubject().toString());
        TeacherEntity teacherEntity = teacherDbRepository.save(te);
    }

    @Override
    public Teacher getTeacherbyId(int id) {

        TeacherEntity te = teacherDbRepository.findById(id).orElse(null);
        return new Teacher(te.getT_id(), te.getUsername(), te.getFirst_name(), te.getLast_name(),
                te.getEmail(), te.getPassword(), SUBJECT.valueOf(te.getSubject()));
    }

    @Override
    public List<Teacher> getAllTeachers() {
        return teacherDbRepository.findAll().stream()
                .map(te -> new Teacher(te.getT_id(), te.getUsername(), te.getFirst_name(), te.getLast_name(),
                        te.getEmail(), te.getPassword(), SUBJECT.valueOf(te.getSubject())))
                .collect(Collectors.toList());
    }

    @Override
    public Teacher login(String uname, String pass) {
        return new Teacher (teacherDbRepository.login(uname, pass));
    }
}
