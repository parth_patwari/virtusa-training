package com.springboot.training.studentservice.model.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "student")
public class StudentEntity {

    @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String first_name;
    private String last_name;
    private String email;

//    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL)
//    @JoinColumn(name = "id", referencedColumnName = "s_id")
//    List<StudentGradesEntity> studentGradesEntities;


    public StudentEntity() {
    }

    public StudentEntity(String username, String first_name, String last_name, String email) {
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
    }
}
