package com.springboot.training.studentservice.repository.student;

import com.springboot.training.studentservice.model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Repository
public class StudentRepositoryInMemoryImpl implements StudentRepository{

    final List<Student> students;
    public StudentRepositoryInMemoryImpl(){
        students = new ArrayList<>();
        students.add(new Student(1, "MJohnson","Mark", "Johnson", "mj@virtusa.com"));
        students.add(new Student(2, "JDoe","John", "Doe", "jd@virtusa.com"));
        students.add(new Student(3, "MJosh","Micheal", "Josh", "mjosh@virtusa.com"));
        students.add(new Student(4, "JKen","Jonathan", "ken", "jken@virtusa.com"));
        students.add(new Student(5, "MLee","Mike", "Lee", "mlee@virtusa.com"));
    }
    @Override
    public List<Student> getAllStudent() {
        return students;    }

    @Override
    public Student getStudentByID(int id) {
        return students.stream().filter(student -> student.getId() == id)
                .findFirst().orElseThrow(NoSuchElementException::new);

    }

    @Override
    public void addStudent(Student student) {
        students.add(student);
    }

    @Override
    public List<Student> getStudentsbyFirstName(String fname) {
        return students.stream().filter(student -> student.getFName().startsWith(fname)).collect(Collectors.toList());
    }

    @Override
    public List<Student> getStudentsbyName(String name) {
        return students.stream().filter(student ->
                (student.getFName().startsWith(name)) || student.getLName()
                        .startsWith(name)).collect(Collectors.toList());
    }

    @Override
    public void deleteStudent(int id) {
        students.removeIf(student -> student.getId() == id);
    }

    @Override
    public void updateStudent(String username, Student student) {
        students.stream().filter(student1 -> student1.getUserName().equals(username))
                .forEach(student1 ->
                {
                    student1.setId(student.getId());
                    student1.setUserName(student.getUserName());
                    student1.setFName(student.getFName());
                    student1.setLName(student.getLName());
                    student1.setEmail(student.getEmail());

                }
                );
        System.out.println(students.stream().filter(student1 -> student1.getUserName().equals(username)).findFirst());
    }
}
