package com.springboot.training.studentservice.service;

import com.springboot.training.studentservice.model.Student;
import com.springboot.training.studentservice.model.entity.StudentEntity;
import com.springboot.training.studentservice.repository.student.StudentDbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

//    @Autowired
//    StudentRepository studentRepository;

    @Autowired
    StudentDbRepository studentDbRepository;

    @Override
    public List<Student> getAllStudents() {
        return studentDbRepository.findAll().stream()
                .map(se -> new Student(se.getId(), se.getUsername(), se.getFirst_name(), se.getLast_name(), se.getEmail()))
                .collect(Collectors.toList());
    }

    @Override
    public int addStudent(Student student) {

        StudentEntity studentEntity = new StudentEntity(student.getUserName(),
                student.getFName(), student.getLName(), student.getEmail());
//        System.out.println(studentEntity.toString());
        studentEntity = studentDbRepository.save(studentEntity);

        return studentEntity.getId();
    }

    @Override
    public Student getStudentByID(int id) {

        StudentEntity studentEntity = studentDbRepository.findById(id).orElse(null);
        return new Student(studentEntity.getId(), studentEntity.getUsername(), studentEntity.getFirst_name()
        ,studentEntity.getLast_name(), studentEntity.getEmail());
    }

    @Override
    public List<Student> getStudentsbyFirstName(String fname) {
        return studentDbRepository.searchByFirstName(fname).stream()
                .map(se -> new Student(se.getId(), se.getUsername(), se.getFirst_name(), se.getLast_name(), se.getEmail()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> getStudentsbyName(String name) {
        return studentDbRepository.searchByName(name).stream()
                .map(se -> new Student(se.getId(), se.getUsername(), se.getFirst_name(), se.getLast_name(), se.getEmail()))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteStudent(int id) {
        StudentEntity studentEntity = studentDbRepository.findById(id).orElse(null);
        if (studentEntity!= null)
            studentDbRepository.delete(studentEntity);
//        studentRepository.deleteStudent(id);
    }

    @Override
    public void updateStudent(String username, Student student) {
        StudentEntity studentEntity = studentDbRepository.searchByUserName(username);
        studentEntity.setUsername(student.getUserName());
        studentEntity.setFirst_name(student.getFName());
        studentEntity.setLast_name(student.getLName());
        studentEntity.setEmail(student.getEmail());
        studentDbRepository.save(studentEntity);


    }
}
