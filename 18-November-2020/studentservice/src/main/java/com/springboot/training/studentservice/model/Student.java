package com.springboot.training.studentservice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Student {
    private int id;
    private String userName;
    private String fName;
    private String lName;
    private String email;
//    private Date dob;
}
