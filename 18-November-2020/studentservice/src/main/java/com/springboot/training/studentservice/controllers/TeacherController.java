package com.springboot.training.studentservice.controllers;


import com.springboot.training.studentservice.model.Teacher;
import com.springboot.training.studentservice.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeacherByid(@PathVariable("id") int id){
        Teacher teacher = teacherService.getTeacherbyId(id);
        if (teacher == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(teacher);
    }

    @PostMapping("/add")
    public List<Teacher> addTeacher(@RequestBody Teacher teacher){
        teacherService.addTeacher(teacher);

        return teacherService.getAllTeachers();
    }
    @PostMapping("/login")
    public ResponseEntity loginTeacher(@RequestParam("username") String username, @RequestParam("pass") String pass){
        System.out.println(username);

        if(teacherService.login(username, pass) == null){
            return (ResponseEntity<Void>) ResponseEntity.status(HttpStatus.UNAUTHORIZED);
        }
        return ResponseEntity.status(HttpStatus.FOUND).body("Successfully Logged In");
    }
}
