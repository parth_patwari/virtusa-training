package com.springboot.training.studentservice.repository.grades;

import com.springboot.training.studentservice.model.entity.StudentGradesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentGradesDbRepository extends JpaRepository<StudentGradesEntity, Integer>{

}
