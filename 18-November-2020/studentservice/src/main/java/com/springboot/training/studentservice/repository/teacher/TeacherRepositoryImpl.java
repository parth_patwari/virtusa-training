package com.springboot.training.studentservice.repository.teacher;

import com.springboot.training.studentservice.model.SUBJECT;
import com.springboot.training.studentservice.model.Teacher;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TeacherRepositoryImpl implements TeacherRepository {

    final List<Teacher> teachers;

    public TeacherRepositoryImpl() {
        this.teachers = new ArrayList<>();
        this.teachers.add(new Teacher(21, "JCooper", "Jake",
                "Cooper", "jcp@virtusa.com", "J123", SUBJECT.HISTORY));
        this.teachers.add(new Teacher(22, "BJones", "Betty",
                "Jones", "bjn@virtusa.com", "B123", SUBJECT.MATHEMATICS));
        this.teachers.add(new Teacher(21, "JCooper", "Erik",
                "Clark", "eclk@virtusa.com", "E123", SUBJECT.SCIENCE));
    }

    @Override
    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    @Override
    public Teacher getTeacherbyId(int id) {
        return teachers.stream().filter(teacher -> teacher.getId() == id).findFirst().orElse(null);
    }

    @Override
    public List<Teacher> getAllTeachers() {
        return teachers;
    }

    @Override
    public Teacher login(String uname, String pass) {
        return teachers.stream().filter(teacher -> (teacher.getUsername().equals(uname)
        && teacher.getPassword().equals(pass))).findFirst().orElse(null);
    }
}
