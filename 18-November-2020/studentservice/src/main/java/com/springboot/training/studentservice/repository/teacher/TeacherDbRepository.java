package com.springboot.training.studentservice.repository.teacher;

import com.springboot.training.studentservice.model.entity.StudentEntity;
import com.springboot.training.studentservice.model.entity.TeacherEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherDbRepository extends JpaRepository<TeacherEntity, Integer> {

    @Query("SELECT s FROM TeacherEntity s WHERE s.username =:username and s.password =:pass")
    TeacherEntity login(@Param("username") String username, @Param("pass") String pass);

}
