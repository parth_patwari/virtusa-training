package com.springboot.training.studentservice.model.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "teacher")
public class TeacherEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int t_id;
    private String username;
    private String first_name;
    private String last_name;
    private String email;
    private String password;
    private String subject;
    public TeacherEntity() {
    }

    public TeacherEntity(String username, String first_name, String last_name, String email, String password, String subject) {
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.password = password;
        this.subject = subject;
    }
}
