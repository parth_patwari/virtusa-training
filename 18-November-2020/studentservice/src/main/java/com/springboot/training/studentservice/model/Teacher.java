package com.springboot.training.studentservice.model;


import com.springboot.training.studentservice.model.entity.TeacherEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Teacher {
        private int id;
        private  String username;
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private SUBJECT subject;

        public Teacher(TeacherEntity teacherEntity){
                this.id = teacherEntity.getT_id();
                this.username = teacherEntity.getUsername();
                this.firstName = teacherEntity.getFirst_name();
                this.lastName = teacherEntity.getLast_name();
                this.email = teacherEntity.getEmail();
                this.password = teacherEntity.getPassword();
                this.subject = SUBJECT.valueOf(teacherEntity.getSubject());
        }
}
