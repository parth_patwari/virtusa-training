package com.springboot.training.studentservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class StudentGrade {
    private int id;
    private SUBJECT subject;
    private int grade;
}
