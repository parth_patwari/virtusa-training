package com.springboot.training.studentservice.controllers;


import com.springboot.training.studentservice.model.SUBJECT;
import com.springboot.training.studentservice.model.StudentGrade;
import com.springboot.training.studentservice.model.entity.StudentGradesEntity;
import com.springboot.training.studentservice.repository.grades.StudentGradesDbRepository;
import com.springboot.training.studentservice.service.StudentGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grades")
public class GradesController {

    @Autowired
    StudentGradeService studentGradeService;

    @GetMapping("/{id}")
    public List<StudentGrade> getStudentGrade(@PathVariable("id") int id){
        List<StudentGradesEntity> studentGradesEntity = studentGradeService.getStudentGrade(id);
        List<StudentGrade> studentGrades= new ArrayList<>();
        for (StudentGradesEntity sge: studentGradesEntity) {
            studentGrades.add(new StudentGrade(sge.getStudentEntity().getId(), SUBJECT.valueOf(sge.getSubject()), sge.getScore()));
        }
        return studentGrades;
    }

    @GetMapping
    public List<StudentGrade> getAllStudentsGrades(){
        return studentGradeService.getStudentGrades().stream()
                .map(sge -> new StudentGrade(sge.getStudentEntity().getId(), SUBJECT.valueOf(sge.getSubject()), sge.getScore())).collect(Collectors.toList());
    }
}
