package com.springboot.training.studentservice.repository.grades;

import com.springboot.training.studentservice.model.StudentGrade;

import java.util.List;

public interface StudentGradesRepository {
    StudentGrade getStudentGrades(int it);
    List<StudentGrade> getAllStudentGrades();
}
