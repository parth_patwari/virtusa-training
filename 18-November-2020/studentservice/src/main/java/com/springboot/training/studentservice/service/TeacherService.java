package com.springboot.training.studentservice.service;

import com.springboot.training.studentservice.model.Teacher;
import com.springboot.training.studentservice.model.entity.TeacherEntity;

import java.util.List;

public interface TeacherService {

    void addTeacher(Teacher teacher);

    Teacher getTeacherbyId(int id);

    List<Teacher> getAllTeachers();

    Teacher login(String uname, String pass);
}
