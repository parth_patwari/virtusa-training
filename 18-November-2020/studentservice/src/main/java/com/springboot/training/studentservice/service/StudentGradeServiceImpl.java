package com.springboot.training.studentservice.service;

import com.springboot.training.studentservice.model.StudentGrade;
import com.springboot.training.studentservice.model.entity.StudentGradesEntity;
import com.springboot.training.studentservice.repository.grades.StudentGradesDbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentGradeServiceImpl implements StudentGradeService {

//    @Autowired
//    StudentGradesRepository studentGradesRepository;

    @Autowired
    StudentGradesDbRepository studentGradesDbRepository;




//    @Override
//    public List<StudentGrade> getStudentGrades() {
//        return studentGradesRepository.getAllStudentGrades();
//    }


    @Override
    public List<StudentGradesEntity> getStudentGrade(int id) {
        return studentGradesDbRepository.findAll().stream()
                .filter(sge -> sge.getStudentEntity().getId() == id).collect(Collectors.toList());
    }

    @Override
    public List<StudentGradesEntity> getStudentGrades() {
        return studentGradesDbRepository.findAll();
    }

}
