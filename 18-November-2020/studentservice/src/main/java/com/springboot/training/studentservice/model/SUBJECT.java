package com.springboot.training.studentservice.model;

public enum SUBJECT {
    MATHEMATICS,
    SCIENCE,
    HISTORY
}
