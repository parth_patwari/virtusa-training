package com.springboot.training.studentservice.service;

import com.springboot.training.studentservice.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudents();

    Student getStudentByID(int id);

    int addStudent(Student student);
    List<Student> getStudentsbyFirstName(String fname);

    List<Student> getStudentsbyName(String name);

    void deleteStudent(int id);

    void updateStudent(String username, Student student);
}
