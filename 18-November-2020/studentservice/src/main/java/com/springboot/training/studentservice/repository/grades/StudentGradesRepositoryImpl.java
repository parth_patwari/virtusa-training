package com.springboot.training.studentservice.repository.grades;

import com.springboot.training.studentservice.model.SUBJECT;
import com.springboot.training.studentservice.model.StudentGrade;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class StudentGradesRepositoryImpl implements StudentGradesRepository {

    final List<StudentGrade> studentgrades;

    public StudentGradesRepositoryImpl(List<StudentGrade> studentgrades) {
        this.studentgrades = new ArrayList<>();
//        Map<SUBJECT, Integer> grades1 = new HashMap<>();
//        grades1.put(SUBJECT.HISTORY, 100);
//        grades1.put(SUBJECT.MATHEMATICS, 100);
//        grades1.put(SUBJECT.SCIENCE, 90);
//        this.studentgrades.add(new StudentGrade(1, grades1));
//
//        Map<SUBJECT, Integer> grades2 = new HashMap<>();
//        grades2.put(SUBJECT.HISTORY, 70);
//        grades2.put(SUBJECT.MATHEMATICS, 80);
//        grades2.put(SUBJECT.SCIENCE, 89);
//        this.studentgrades.add(new StudentGrade(2, grades2));
//
//        Map<SUBJECT, Integer> grades3 = new HashMap<>();
//        grades3.put(SUBJECT.HISTORY, 80);
//        grades3.put(SUBJECT.MATHEMATICS, 90);
//        grades3.put(SUBJECT.SCIENCE, 87);
//        this.studentgrades.add(new StudentGrade(3, grades3));
    }

    @Override
    public StudentGrade getStudentGrades(int id) {

        return studentgrades.stream().filter(studentGrade -> studentGrade.getId() == id).findFirst().orElse(null);
    }

    @Override
    public List<StudentGrade> getAllStudentGrades() {
        return studentgrades;
    }
}
