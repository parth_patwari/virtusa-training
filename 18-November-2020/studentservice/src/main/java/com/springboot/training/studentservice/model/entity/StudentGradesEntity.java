package com.springboot.training.studentservice.model.entity;

import com.springboot.training.studentservice.model.SUBJECT;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Map;
@Getter
@Setter
@ToString
@Entity
@Table(name = "studentgrades")
public class StudentGradesEntity {


        @ManyToOne
        @JoinColumn(name="s_id", referencedColumnName = "id", insertable = false)
        private StudentEntity studentEntity;

//        private int s_id;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int grades_id;

        private String subject;
        private int score;

        public StudentGradesEntity() {
        }

//        public StudentGradesEntity( int s_id, String subject, int score) {
//            this.s_id = s_id;
//            this.subject = subject;
//            this.score = score;
//        }

}
