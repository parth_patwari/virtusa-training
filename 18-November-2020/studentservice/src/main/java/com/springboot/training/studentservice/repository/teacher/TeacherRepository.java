package com.springboot.training.studentservice.repository.teacher;

import com.springboot.training.studentservice.model.Teacher;

import java.util.List;

public interface TeacherRepository {

    void addTeacher(Teacher teacher);

    Teacher getTeacherbyId(int id);

    List<Teacher> getAllTeachers();

    Teacher login(String uname, String pass);
}
