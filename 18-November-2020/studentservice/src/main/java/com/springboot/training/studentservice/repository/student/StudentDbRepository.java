package com.springboot.training.studentservice.repository.student;

import com.springboot.training.studentservice.model.Student;
import com.springboot.training.studentservice.model.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentDbRepository extends JpaRepository<StudentEntity, Integer> {

    @Query("SELECT s FROM StudentEntity s WHERE s.first_name LIKE CONCAT('%', :param, '%') or s.last_name LIKE  CONCAT('%', :param, '%')")
    List<StudentEntity> searchByName(@Param("param") String param);

    @Query("SELECT s FROM StudentEntity s WHERE s.username =:param")
    StudentEntity searchByUserName(@Param("param") String param);

    @Query("SELECT s FROM StudentEntity s WHERE s.first_name =:param")
    List<StudentEntity> searchByFirstName(@Param("param") String param);


}
