package com.springboot.training.studentservice.service;

import com.springboot.training.studentservice.model.StudentGrade;
import com.springboot.training.studentservice.model.entity.StudentGradesEntity;

import java.util.List;


public interface StudentGradeService {

   List<StudentGradesEntity> getStudentGrade(int id);

    List<StudentGradesEntity> getStudentGrades();
}
