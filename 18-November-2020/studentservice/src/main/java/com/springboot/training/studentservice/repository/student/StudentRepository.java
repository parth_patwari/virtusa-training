package com.springboot.training.studentservice.repository.student;
import com.springboot.training.studentservice.model.Student;
import java.util.List;

public interface StudentRepository {
    List<Student> getAllStudent();

    Student getStudentByID(int id);

    void addStudent(Student student);

    List<Student> getStudentsbyFirstName(String fname);

    List<Student> getStudentsbyName(String name);

    void deleteStudent(int id);

    void updateStudent(String username, Student student);
}
