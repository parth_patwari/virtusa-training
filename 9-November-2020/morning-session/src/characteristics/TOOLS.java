package characteristics;

public enum TOOLS {
    SWORD,
    GUN,
    PLOUGH,
    BINOCULARS,
    HAMMER,
    BOW_ARROW,
    TOY
}
