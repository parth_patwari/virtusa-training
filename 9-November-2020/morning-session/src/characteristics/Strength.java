package characteristics;

public enum Strength {
    HIGH,
    MEDIUM,
    LOW
}
