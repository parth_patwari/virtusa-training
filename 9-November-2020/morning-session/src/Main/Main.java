package Main;

import NPCFactory.*;
import model.ICharacter;
import model.NPCType;

public class Main {
    public static void main(String[] args) {
        NPCFactory npcFactory = new NPCFactory(NPCType.SOLDIER_GUN);
        ICharacter iCharacter = npcFactory.getCharacter();
        iCharacter.displayCharacter();

        NPCPanicFactory npcPanicFactory = new NPCPanicFactory();
        npcPanicFactory.createPanic();
    }
}
