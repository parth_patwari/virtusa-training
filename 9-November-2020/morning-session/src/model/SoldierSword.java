package model;

import characteristics.*;

public class SoldierSword extends NonPlayingCharacters{

    public SoldierSword(){
        super(SKILL.HIGH, INTELLIGENCE.LOW, SPEED.HIGH, Strength.HIGH, TOOLS.SWORD);
    }
}
