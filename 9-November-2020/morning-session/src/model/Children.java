package model;

import characteristics.*;

public class Children extends NonPlayingCharacters implements IMove{

    public Children(){

        super(SKILL.LOW, INTELLIGENCE.LOW, SPEED.LOW, Strength.LOW, TOOLS.TOY);
    }

    @Override
    public void displayCharacter(){
        System.out.println("Child:\n" +
                "Skill: " +super.skill.toString() + "\n" +
                "Speed: " +super.speed.toString() + "\n" +
                "Tool: " +super.tools.toString() + "\n" +
                "Strength: " +super.strength.toString() + "\n" + "");
    }

    @Override
    public void move() {
        System.out.println("Child is moving");
    }
}

