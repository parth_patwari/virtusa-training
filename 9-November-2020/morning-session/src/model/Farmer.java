package model;

import characteristics.*;

public class Farmer extends NonPlayingCharacters implements IMove{

    public Farmer(){
        super(SKILL.MEDIUM, INTELLIGENCE.LOW, SPEED.LOW, Strength.MEDIUM, TOOLS.PLOUGH);
    }

    @Override
    public void displayCharacter(){
        System.out.println("Farmer:\n" +
                "Skill: " +super.skill.toString() + "\n" +
                "Speed: " +super.speed.toString() + "\n" +
                "Tool: " +super.tools.toString() + "\n" +
                "Strength: " +super.strength.toString() + "\n" + "");
    }

    @Override
    public void move() {
        System.out.println("Farmer is moving");
    }
}
