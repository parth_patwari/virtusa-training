package model;

import characteristics.*;

public class Female_Builder extends NonPlayingCharacters implements IMove{

    public Female_Builder(){
        super(SKILL.LOW, INTELLIGENCE.LOW, SPEED.MEDIUM, Strength.LOW, TOOLS.HAMMER);
    }

    @Override
    public void displayCharacter(){
        System.out.println(" Female Village Builder:\n" +
                "Skill: " +super.skill.toString() + "\n" +
                "Speed: " +super.speed.toString() + "\n" +
                "Tool: " +super.tools.toString() + "\n" +
                "Strength: " +super.strength.toString() + "\n" + "");
    }

    @Override
    public void move() {
        System.out.println("Female Builder is moving");
    }
}
