package model;

import characteristics.*;

public abstract class Character implements ICharacter{
    SKILL skill;
    INTELLIGENCE intelligence;
    SPEED speed;
    Strength strength;
    TOOLS tools;


    public Character(SKILL skill, INTELLIGENCE intelligence, SPEED speed, Strength strength, TOOLS tools) {
        this.skill = skill;
        this.intelligence = intelligence;
        this.speed = speed;
        this.strength = strength;
        this.tools = tools;
    }



}
