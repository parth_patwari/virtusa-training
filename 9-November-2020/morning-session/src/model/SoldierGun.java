package model;

import characteristics.*;

public class SoldierGun extends NonPlayingCharacters{

    public SoldierGun() {
        super(SKILL.HIGH, INTELLIGENCE.LOW, SPEED.HIGH, Strength.HIGH, TOOLS.GUN);
    }

    @Override
    public void displayCharacter(){
        System.out.println("Solider with gun:\n" +
                "Skill: " +super.skill.toString() + "\n" +
                "Speed: " +super.speed.toString() + "\n" +
                "Strength: " +super.strength.toString() + "\n" + "");
    }
}
