package model;

import characteristics.*;

public class Builder extends NonPlayingCharacters implements IMove{

    public Builder(){
        super(SKILL.LOW, INTELLIGENCE.LOW, SPEED.MEDIUM, Strength.MEDIUM, TOOLS.HAMMER);
    }

    @Override
    public void displayCharacter(){
        System.out.println(" Village Builder:\n" +
                "Skill: " +super.skill.toString() + "\n" +
                "Speed: " +super.speed.toString() + "\n" +
                "Tool: " +super.tools.toString() + "\n" +
                "Strength: " +super.strength.toString() + "\n" + "");
    }

    @Override
    public void move() {
        System.out.println("Builder is moving");
    }
}
