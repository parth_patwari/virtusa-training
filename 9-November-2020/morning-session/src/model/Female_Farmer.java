package model;

import characteristics.*;

public class Female_Farmer extends NonPlayingCharacters implements IMove{

    public Female_Farmer(){
        super(SKILL.MEDIUM, INTELLIGENCE.MEDIUM, SPEED.LOW, Strength.LOW, TOOLS.PLOUGH);
    }

    @Override
    public void displayCharacter(){
        System.out.println("Female Village Farmer:\n" +
                "Skill: " +super.skill.toString() + "\n" +
                "Speed: " +super.speed.toString() + "\n" +
                "Tool: " +super.tools.toString() + "\n" +
                "Strength: " +super.strength.toString() + "\n" + "");
    }

    @Override
    public void move() {
        System.out.println("Female Farmer is moving");
    }
}
