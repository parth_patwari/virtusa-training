package model;

import characteristics.*;

public class SoldierSpy  extends  NonPlayingCharacters{

    public SoldierSpy(){
        super(SKILL.HIGH, INTELLIGENCE.HIGH, SPEED.HIGH, Strength.MEDIUM, TOOLS.BINOCULARS);
    }
}
