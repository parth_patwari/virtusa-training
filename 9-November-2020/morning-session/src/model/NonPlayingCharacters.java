package model;

import characteristics.*;

public class NonPlayingCharacters extends Character{

//    private NPCType npcType;


    public NonPlayingCharacters(SKILL skill, INTELLIGENCE intelligence, SPEED speed, Strength strength, TOOLS tools) {
        super(skill, intelligence, speed, strength, tools);
    }

    @Override
    public void displayCharacter() {

    }
}
