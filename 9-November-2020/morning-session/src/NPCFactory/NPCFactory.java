package NPCFactory;

import model.*;


public class NPCFactory {

    NPCType npcType;
    public NPCFactory(NPCType npcType){
        this.npcType = npcType;
    }

    public ICharacter getCharacter(){
        if( npcType == NPCType.SOLDIER_GUN){
            return new SoldierGun();
        }
        else if (npcType == NPCType.SOLDIER_SWORD){
            return new SoldierSword();
        }
        else if (npcType == NPCType.SOLDIER_SPY){
            return new SoldierSpy();
        }
        else if (npcType == NPCType.VILLAGE_FARMER){
            return new Farmer();
        }
        return null;
    }
}
