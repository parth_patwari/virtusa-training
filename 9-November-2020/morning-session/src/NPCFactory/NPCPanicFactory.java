    package NPCFactory;

    import model.*;
    import java.util.ArrayList;
    import java.util.List;
    import java.util.Random;

    public class NPCPanicFactory {

        public NPCPanicFactory(){}

        private NPCType[] npcTypeArr = {NPCType.VILLAGE_BUILDER, NPCType.VILLAGE_FARMER, NPCType.CHILDREN,
                NPCType.FEMALE_BUILDER, NPCType.FEMALE_FARMER};

        private List<IMove> getMovingNPC(){
            List<IMove> movingNPCs = new ArrayList<IMove>();
            for(int i=0; i<10; i++) {
                NPCType npcType = npcTypeArr[new Random().nextInt(npcTypeArr.length)];
                if(npcType == NPCType.VILLAGE_FARMER)movingNPCs.add(new Farmer());
                if(npcType == NPCType.VILLAGE_BUILDER)movingNPCs.add(new Builder());
                if(npcType == NPCType.FEMALE_BUILDER)movingNPCs.add(new Female_Builder());
                if(npcType == NPCType.FEMALE_FARMER)movingNPCs.add(new Female_Farmer());
                if(npcType == NPCType.CHILDREN)movingNPCs.add(new Children());
            }
            return movingNPCs;
        }

        public void createPanic(){

            System.out.println("\n------------------------------------------------------------");
            System.out.println("Attack on Village.... Panic Created");
            List<IMove> imove = getMovingNPC();
            for(IMove i:imove){
                i.move();
            }
        }

    }
