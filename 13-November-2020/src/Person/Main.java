package Person;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        PersonCreator personCreator = new PersonCreator();
        personCreator.createPerson();
//        personCreator.displayPerson();
        long Currenttime = System.currentTimeMillis();
//        List<Person> oldPersons = personCreator.oldPerson();
        List<List<Person>> personPartitionList = splitlist(personCreator.personList, 5);
        Runnable r1 = new PersonThread(personPartitionList.get(0), personCreator);
        Runnable r2 = new PersonThread(personPartitionList.get(1), personCreator);
        Runnable r3 = new PersonThread(personPartitionList.get(2), personCreator);
        Runnable r4 = new PersonThread(personPartitionList.get(3), personCreator);
        Runnable r5 = new PersonThread(personPartitionList.get(4), personCreator);

        // creates a thread pool with MAX_T no. of
        // threads as the fixed pool size(Step 2)
        ExecutorService pool = Executors.newFixedThreadPool(5);

        // passes the Task objects to the pool to execute (Step 3)
        pool.execute(r1);
        pool.execute(r2);
        pool.execute(r3);
        pool.execute(r4);
        pool.execute(r5);

        // pool shutdown ( Step 4)
        pool.shutdown();
        while(!pool.isTerminated()){}
        System.out.println(System.currentTimeMillis() - Currenttime);

//        System.out.println(personCreator.oldPersons.size());
    }
    public static <T> List<List<T>> splitlist(List<T> originalList, int partitionSize ){
        List<List<T>> partitions = new LinkedList<>();
        for (int i = 0; i < originalList.size(); i += (originalList.size()/partitionSize)) {
            partitions.add(originalList.subList(i, i + (originalList.size()/partitionSize)));

        }
        return partitions;
    }
}
