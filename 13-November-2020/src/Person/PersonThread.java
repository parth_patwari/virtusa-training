package Person;

import java.util.List;

public class PersonThread implements Runnable {
    private List<Person> personList;
    private PersonCreator personCreator;
    List<Person> splitList;
    public PersonThread(List<Person> personList, PersonCreator personCreator)
    {
        this.personList = personList;
        this.personCreator = personCreator;
    }

    // Prints task name and sleeps for 1s
    // This Whole process is repeated 5 times
    public void run()
    {
        try
        {
            this.splitList = this.personCreator.oldPerson(this.personList);
            System.out.println(splitList.size() +"," + personList.size());
        }

        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}