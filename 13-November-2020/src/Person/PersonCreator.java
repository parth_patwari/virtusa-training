package Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PersonCreator{

    List<Person> personList = new ArrayList<>();
    public void createPerson(){
        Random random = new Random();
        for(int i = 0; i<10000; i++){
            this.personList.add(new Person(("ABC"+i), "PQR"+ i, random.nextInt(100)));
        }
    }
    public void displayPerson(){
        for(Person person:this.personList){
            System.out.println(person.fname+ " "+ person.lname + " " + person.age);
        }
    }

    public boolean isOldPerson(Person person) throws InterruptedException {
        Thread.sleep(2);
        if(person.age > 65){
            return true;
        }
        else{
            return false;
        }
    }

    public List<Person> oldPerson(List<Person> personList) throws InterruptedException {
        List<Person> oldPersons = new ArrayList<>();
        for(Person person: personList){
            if (isOldPerson(person) == true){
                oldPersons.add(person);
            }
        }
        return oldPersons;
    }

}
