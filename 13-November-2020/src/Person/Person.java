package Person;

public class Person {
    String fname;
    String lname;
    int age;

    public Person(String fname, String lname, int age){
        this.fname = fname;
        this.lname = lname;
        this.age = age;
    }
}
