package singleton;

public class Singleton {
    public static void main(String[] args) {
        SingletonDemo singletonDemo = SingletonDemo.singletonInstance;
        System.out.println(singletonDemo);

        SingletonDemo singletonDemo1 = singletonDemo.singletonInstance;

        System.out.println(singletonDemo.getA());

        System.out.println(singletonDemo1.getA());

        singletonDemo.setA(22);

        System.out.println(singletonDemo.getA());

        System.out.println(singletonDemo1.getA());
    }
}
