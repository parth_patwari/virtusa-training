package singleton;

public enum SingletonDemo {
    singletonInstance;
    int a;
    public void setA(int a){
        this.a = a;
    }

    public int getA(){
        return this.a;
    }
}
