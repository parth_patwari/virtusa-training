import java.util.List;

public class ConsumerThread implements Runnable {
    String name;

    ProducerConsumer pc;
    public ConsumerThread(ProducerConsumer producerConsumer, String name) {
        this.pc = producerConsumer;
        this.name = name;
    }


    @Override
    public void run() {
        try {
            pc.consume(this.name);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

