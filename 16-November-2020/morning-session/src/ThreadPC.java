import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ThreadPC {
    private static volatile boolean exit = false;
    public static void main(String[] args) throws InterruptedException {
        ProducerConsumer producerConsumer = new ProducerConsumer();
//
//
//        Thread producer_thread = new Thread(() -> {
//            try {
//                while (!exit) {
//                    producerConsumer.produce();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });
//        Thread consumer_thread = new Thread(() -> {
//            try {
//                while (!exit) {
//                    producerConsumer.consume();
//                }
//
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });
//
//        Thread consumer_thread1 = new Thread(() -> {
//            try {
//                while (!exit) {
//                    producerConsumer.consume();
//                }
//
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });

       Thread producerThread = new Thread((Runnable) new ProducerThread(producerConsumer));
       Thread consumerThread = new Thread((Runnable) new ConsumerThread(producerConsumer, "Consumer 1"));
       Thread consumerThread1 = new Thread((Runnable) new ConsumerThread(producerConsumer, "Consumer 2"));
        Thread consumerThread2 = new Thread((Runnable) new ConsumerThread(producerConsumer, "Consumer 3"));

//       producerThread.start();
//       consumerThread.start();
//       consumerThread1.start();
//       consumerThread2.start();
        ExecutorService pool = Executors.newFixedThreadPool(4);
        pool.execute(producerThread);
        pool.execute(consumerThread);
        pool.execute(consumerThread1);
        pool.execute(consumerThread2);
//       producerThread.run();
//       consumerThread.run();
//       consumerThread1.run();
        long startTime = System.currentTimeMillis();
//        consumer_thread.start();
//        producer_thread.start();
//        consumer_thread.start();
//        consumer_thread1.start();
            Thread.sleep(2120);
//            exit = true;
//        pool.shutdown();
//        while(!pool.isTerminated()){}
        System.out.println("Threading Complete:"+ producerConsumer.getList());
        System.exit(0);

    }

}
