import java.util.List;

public class ProducerThread implements Runnable{
//    List<Integer> list;
    ProducerConsumer pc;
    public ProducerThread(ProducerConsumer producerConsumer) {
        this.pc = producerConsumer;
    }


    @Override
    public void run() {
        try {
            pc.produce();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
