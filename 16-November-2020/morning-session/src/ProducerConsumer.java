import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProducerConsumer {
    List<Integer> list = (new ArrayList<>());
    int buffer_size = 10;

    public List<Integer> getList() {
        return list;
    }

    public void produce() throws InterruptedException
    {

        int value = 0;
        int count =0;
        while (true) {
            synchronized (this)
            {
                while (list.size() == buffer_size)
//                    Thread.sleep(20);
                    wait();
                System.out.println("Producer has produced product:" + value);
                list.add(value++);
//                wait();
                notify();
                count++;
            }
        }
    }
    public void consume(String name) throws InterruptedException
    {
        int count = 0;
        while (true) {
            synchronized (this)
            {
                while (list.size() ==0)
                    wait();
                int val = list.remove(0);
                Thread.sleep(30);
                System.out.println("-------------------\n"+name+" has consumed product:" + val +"\n");
                wait();

                notify();
                count++;
            }

        }
    }
}
