package LSP;

public class LSP {
    public static void main(String[] args) {

        Person person = new Student("ABC", "student");
        Teacher teacher = new Teacher("QWE", "teacher");

        person.display();

        teacher.display();
        teacher.show_benefits();
    }
}
