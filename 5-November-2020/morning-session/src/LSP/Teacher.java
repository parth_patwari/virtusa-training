package LSP;

public class Teacher extends Person implements EmployeeBenefits{
    public Teacher(String name, String type) {
        super(name, type);
    }

    @Override
    public void show_benefits() {
        System.out.println("Teacher");
    }
}
