package LSP;

public class Student extends Person{


    public Student(String name, String type) {
        super(name, type);
    }

    @Override
    public void display() {
        System.out.println("Student Class");
    }
}
