package srp;

import java.util.*;

public class SRP {
    public static void main(String[] args) {
        Accounts acc1 = new Accounts((long) 123, "ABC", 10000000);
        Accounts acc2 = new Accounts((long) 456, "PQR", 10000000);
        Accounts acc3 = new Accounts((long) 789, "XYZ", 10000000);
        List<Accounts> accounts = new ArrayList<Accounts>();
        accounts.add(acc1);
        accounts.add(acc2);
        accounts.add(acc3);

        Person p1 = new Person("A123", "John", "Doe", 34, accounts);


        PrintPerson printPerson = new PrintPerson(p1);
        printPerson.printPersonName();

    }
}
