package ocp;

import java.util.List;

public class Person {
    private final String id;
    private String fname;
    private String lname;
    private int age;
    private List<Accounts> accounts;

    public Person(String id, String fname, String lname, int age, List<Accounts> accounts){
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.accounts = accounts;
    }

    public String getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public int getAge() {
        return age;
    }

    public List<Accounts> getAccounts() {
        return accounts;
    }
}

