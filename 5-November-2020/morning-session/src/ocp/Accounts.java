package ocp;

public class Accounts {
    private final Long accountid;
    private String accoutname;
    private long balance;

    public Accounts(Long accountid, String accoutname, long balance) {
        this.accountid = accountid;
        this.accoutname = accoutname;
        this.balance = balance;
    }

    public Long getAccountid() {
        return accountid;
    }


    public String getAccoutname() {
        return accoutname;
    }

    public void setAccoutname(String accoutname) {
        this.accoutname = accoutname;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
