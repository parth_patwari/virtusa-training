package ocp;


import java.util.ArrayList;
import java.util.List;

public class OCP {
    public static void main(String[] args) {

        Accounts acc1 = new Accounts((long) 123, "ABC", 10000000);
        Accounts acc2 = new Accounts((long) 456, "PQR", 10000000);
        Accounts acc3 = new Accounts((long) 789, "XYZ", 10000000);
        List<Accounts> accounts = new ArrayList<Accounts>();
        accounts.add(acc1);
        accounts.add(acc2);
        accounts.add(acc3);
        Job p1 = new Job("A123", "John", "Doe", 34, accounts, "Virtusa", 10000);
        System.out.println(p1.getId()+", "+ p1.getFname());
        System.out.println("Job:");
        System.out.println("Company Name:"+p1.getCompany_name() +" and Salary:"+p1.getSalary());
    }
}
