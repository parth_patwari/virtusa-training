package ocp;

import java.util.List;

// extends the person instead of modifying the person cl
public class Job extends Person{
    private String company_name;
    private double salary;
    public Job(String id, String fname, String lname, int age, List<Accounts> accounts, String company_name, double salary) {
        super(id, fname, lname, age, accounts);
        this.company_name = company_name;
        this.salary = salary;
    }

    public String getCompany_name() {
        return company_name;
    }

    public double getSalary() {
        return salary;
    }
}