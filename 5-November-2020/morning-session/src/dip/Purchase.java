package dip;

public class Purchase {
    private Payment payment;
    public Purchase(Payment payment){
        this.payment = payment;
    }
    void make_payment(){
        payment.make_payment();
    }
}
