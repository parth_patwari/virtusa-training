package dip;

public class CreditCardPayment implements Payment{

    @Override
    public void make_payment() {
        System.out.println("Payment Done with Credit Card");
    }
}
