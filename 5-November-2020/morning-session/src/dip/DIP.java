package dip;

public class DIP {
    public static void main(String[] args) {
        Payment payment = new CreditCardPayment();

        Purchase purchase = new Purchase(payment);

        purchase.make_payment();
    }
}
