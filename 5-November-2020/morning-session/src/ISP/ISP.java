package ISP;

public class ISP {
    public static void main(String[] args) {
        Car car = new Car();
        MotorBike motorBike = new MotorBike();

        System.out.println("Implementing Car Service");
        car.checkoil();
        car.checkwheels();
        car.interiorclean();
        car.wash();

        System.out.println("\n-----------------------------------------------------------------------\n");

        System.out.println("Implementing Motorbike Service");
        motorBike.checkoil();
        motorBike.wash();
    }
}
