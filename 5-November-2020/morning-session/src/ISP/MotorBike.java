package ISP;

public class MotorBike implements Wash, CheckOil{

    @Override
    public void wash() {
        System.out.println("Wash Motorbike.");
    }

    @Override
    public void checkoil() {
        System.out.println("Check oil for motorbike.");
    }
}
