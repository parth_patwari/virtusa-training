package ISP;

public class Car implements Wash, WheelAlignment, InteriorCleaning, CheckOil{

    @Override
    public void wash() {
        System.out.println("Wash Car.");
    }

    @Override
    public void interiorclean() {
        System.out.println("Clean car from inside.");
    }

    @Override
    public void checkoil() {
        System.out.println("Check oil for car.");
    }

    @Override
    public void checkwheels() {
        System.out.println("Check wheel Alignment of car");
    }
}
