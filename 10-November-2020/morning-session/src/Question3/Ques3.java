package Question3;

import java.util.HashMap;
import java.util.Map;

public class Ques3 {
    private Map<String, String> map = new HashMap<>();

    public Map<String, String> getMap() {
        return map;
    }

    public Ques3(){
        this.generateMap();
    }
    private void generateMap(){
        this.map.put("Key1", "Value1");
        this.map.put("Key2", "Value2");
        this.map.put("Key3", "Value3");
        this.map.put("Key4", "Value4");
        this.map.put("Key5", "Value5");
        this.map.put("Key6", "Value6");
    }
    public static String findValue(Map<String, String> map, String searchKey){
        String value = "NOT_FOUND";
        if(map.containsKey(searchKey)){
            value = map.get(searchKey);
        }
        return value;
    }
    public static String findValueStream(Map<String, String> map, String searchKey){
        return map.entrySet().stream().filter(k -> searchKey.equals(k.getKey()))
                .map(e -> e.getValue()).findFirst().orElse("Not_Found");
    }

    public void print(String key){
        System.out.println(findValue(this.map, key));
    }

    public void printStream(String key){
        System.out.println(findValueStream(this.map, key));
    }
}