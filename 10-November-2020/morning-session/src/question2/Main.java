package question2;

public class Main {
    public static void main(String[] args) {
        Ques2 ques2 = new Ques2();
        ques2.runPrint();
        System.out.println();
        ques2.getMap().entrySet().stream().forEach(v -> System.out.println(v.getKey()+", "+v.getValue()));
    }
}
