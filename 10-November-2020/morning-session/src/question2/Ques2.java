package question2;

import com.sun.javafx.collections.MappingChange;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Ques2 {
    // LinkedHashMap to maintain insertion order
    private Map<String, String> map = new LinkedHashMap<>();

    public Map<String, String> getMap() {
        return map;
    }

    public Ques2(){
        this.setMap();
    }
    private void setMap(){
        this.map.put("Key1", "Value1");
        this.map.put("Key2", "Value2");
        this.map.put("Key3", "Value3");
        this.map.put("Key4", "Value4");
        this.map.put("Key5", "Value5");
        this.map.put("Key6", "Value6");
    }
    public static void printMapKeyValue(Map<String, String> map){
        for (Map.Entry  <String,String> entry : map.entrySet())
            System.out.println("KEY: " + entry.getKey() +
                    ", VALUE:" + entry.getValue());
    }
    public void runPrint(){
        printMapKeyValue(this.map);
    }
}
