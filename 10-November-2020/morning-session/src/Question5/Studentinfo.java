package Question5;

import java.util.*;
import java.util.stream.Collectors;

public class Studentinfo{
    public static Map<Subject, List<Student>> getBestScores(List<Student> students) {
        Map<Subject, List<Student>> map = new HashMap<>();
        for(Student student: students){
            for(Map.Entry<String, Integer> entry : student.report.entrySet()){
                Subject subject1 = Subject.valueOf(entry.getKey());
                if (map.containsKey(subject1) ){
                    int score = map.get(subject1).get(0).report.get(entry.getKey());
                    if (score < entry.getValue()) {
                        map.get(subject1).set(0, student);
                    } else if (score == entry.getValue()) {
                        String id = "";
                        if (map.get(subject1).get(0).id.compareTo(student.id) == -1) {
                            map.get(subject1).set(0, student);
                        }
                    }
                }
                else{
                    map.put(subject1, new ArrayList<>());
                    map.get(subject1).add(student);
                }
            }

        }
        return map;
    }

    public List<Student> createStudentsData(){
        List<Student> students = new ArrayList<>();
        for(int i =0; i< 5; i++){
            Map<String, Integer> score = new HashMap<>();
            for(Subject subject: Subject.values()){
                score.put(subject.toString(), new Random().nextInt(100));
            }
            students.add(new Student(String.valueOf(i), score));
        }
        return students;
    }

    public void test(List<Student> students){
        Map<Subject, List<Student>> bestScores = getBestScores(students);
        for(Map.Entry<Subject, List<Student>> entry: bestScores.entrySet()){
            System.out.println("Subject: " + entry.getKey().toString() +
                    " with top score for Student: " + entry.getValue());
        }
    }
    public void testStream(List<Student> students){
        Map<Subject, List<Student>> bestScores = getBestScoresStreams(students);
        for(Map.Entry<Subject, List<Student>> entry: bestScores.entrySet()){
            System.out.println("Subject: " + entry.getKey().toString() +
                    " with top score for Student: " + entry.getValue().get(0));
        }
    }

    public static Map<Subject, List<Student>> getBestScoresStreams(List<Student> students1) {
        Map<Subject, List<Student>> map = new HashMap<>();
        for (Subject subject:Subject.values()) {
            int score = students1.stream()
                    .mapToInt(student -> student.report.get(subject.toString()))
                    .max().orElseThrow(NoSuchElementException::new);
//            System.out.println(subject.toString()+": "+ score);
//            map.put(subject, students1.stream().filter(student -> student.report.get(subject).equals(score))
//                    .sorted(Comparator.comparing(student -> student.id)).collect(Collectors.toList()));

            List<Student> s = students1.stream().filter(student1 -> student1.report.get(subject.toString())>=score)
                    .sorted(Comparator.comparing(student -> student.id))
                    .collect(Collectors.toList());
//            System.out.println(s);

            map.put(subject, s);

        }
        return map;
    }

}
class Student {
    String id;
    Map<String, Integer> report;
    public Student(String id, Map<String, Integer> report) {
        this.id = id;
        this.report = report;
    }

    @Override
    public String toString() {
        return "Student{" + "id='" + id + '}';
    }

}
enum Subject {
    SCIENCE, MATHEMATICS, LANGUAGE, HISTORY
}



