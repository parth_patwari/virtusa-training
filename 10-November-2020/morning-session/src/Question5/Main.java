package Question5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Studentinfo studentinfo = new Studentinfo();
        List<Student> students = studentinfo.createStudentsData();
//        studentinfo.test(students);


        // After Streams

        List<Student> students1 = new ArrayList<>();

        //Student 1
        Map<String, Integer> report1 = new HashMap<>();
        report1.put(Subject.MATHEMATICS.toString(), 120);
        report1.put(Subject.SCIENCE.toString(), 10);
        report1.put(Subject.LANGUAGE.toString(), 10);
        report1.put(Subject.HISTORY.toString(), 20);
        Student student1 = new Student("12", report1);
        //Student 2
        Map<String, Integer> report2 = new HashMap<>();
        report2.put(Subject.MATHEMATICS.toString(), 120);
        report2.put(Subject.SCIENCE.toString(), 120);
        report2.put(Subject.LANGUAGE.toString(), 120);
        report2.put(Subject.HISTORY.toString(), 120);
        Student student2 = new Student("22", report2);

        //Student 3
        Map<String, Integer> report3 = new HashMap<>();
        report3.put(Subject.MATHEMATICS.toString(), 120);
        report3.put(Subject.SCIENCE.toString(), 130);
        report3.put(Subject.LANGUAGE.toString(), 10);
        report3.put(Subject.HISTORY.toString(), 120);
        Student student3 = new Student("32", report3);

        students1.add(student1);
        students1.add(student2);
        students1.add(student3);

        studentinfo.testStream(students1);
    }
}
