package Question6;

import java.nio.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.stream.Collectors;

public class Ques6 {
    public static void main(String[] args) throws IOException {
//        try(PrintWriter pw = new PrintWriter("C:\\training\\daily_exercises\\virtusa-training\\" +
//                "10-November-2020\\morning-session\\src\\Question6\\Output.txt")) {
//            File f = new File("C:\\training\\daily_exercises\\virtusa-training\\10-November-2020\\" +
//                    "morning-session\\src\\Question6\\Example");
//            String[] s = f.list();
//            if (s == null) {
//                throw new IOException("Directory doesn't exist: " + f);
//            }
//            for (String s1 : s)
//            {
//                File f1 = new File(f, s1);
//                if (!f1.isFile()) {
//                    continue;
//                }
//                try (Reader reader = new FileReader(f1);
//                     BufferedReader br = new BufferedReader(reader)) {
//                    String line = "";
//                    while((line= br.readLine()) != null){
//                        pw.println(line.toUpperCase());
//                    }
//                }
//            }
//        } catch (Throwable e) {
//            System.out.println("An error occurred while performing the function" + e);
//        }
        try {
            String output = "C:\\training\\daily_exercises\\virtusa-training\\" +
                    "10-November-2020\\morning-session\\src\\Question6\\Output.txt";

            Path filepath = Paths.get("C:\\training\\daily_exercises\\virtusa-training\\10-November-2020\\" +
                    "morning-session\\src\\Question6\\Example");
            Path output1 = Paths.get(output);

            Files.write(output1, Files.list(filepath).map(file ->

            {
                String string = "";
                try {
                    string = Files.lines(file).collect(Collectors.toList()).stream()
                            .map(line -> line.toUpperCase() +"\n").collect(Collectors.joining());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return string;
            }

            ).collect(Collectors.joining()).getBytes());
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }}