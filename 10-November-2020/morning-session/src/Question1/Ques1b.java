package Question1;

import java.util.List;
import java.util.ArrayList;

public class Ques1b {
    private List<List<String>> createlistofListofString(){
        List<List<String>> listList= new ArrayList<>();

        // create innerList
        for(int i=0; i<3; i++){
            List<String> innerList = new ArrayList<>();
            innerList.add("String"+i+"1");
            innerList.add("String"+i+"2");
            innerList.add("String"+i+"3");

            // add inner list to main list
            listList.add(innerList);
        }
        return listList;
    }
    public static List<String> getFlattenList(List<List<String>>
                                                      listOfListOfString){
        List<String> flattenedList = new ArrayList<>();
        for(List innerList: listOfListOfString){

            for(Object string: innerList){
                flattenedList.add(string.toString());
            }
        }
        return flattenedList;
    }
    public List<List<String>> getList(){
        return createlistofListofString();
    }
    public void displayFlattenedList(){

        List<String> flattenedList = getFlattenList(createlistofListofString());

        System.out.println("The flattened List:");
        for(String string:flattenedList){
            System.out.println(string);
        }
    }

}
