package Question1;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        // question 1 a
        Ques1a ques1a = new Ques1a();
        System.out.println("Question 1 a");
        System.out.println(ques1a.averageOfIntegers()+"\n");
        double ans = ques1a.getIntegers().stream().mapToDouble(x -> x).average().orElse(0.0);
        System.out.println(ans);
        // question 1 b
        Ques1b ques1b = new Ques1b();
        System.out.println("Question 1 b");
        ques1b.displayFlattenedList();
        ques1b.getList().stream().flatMap(Collection::stream).collect(Collectors.toList()).forEach(x -> System.out.println(x));
    }

}
