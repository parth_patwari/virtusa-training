package Question1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ques1a {
    private List<Integer> integers = new ArrayList<Integer>();

    public List<Integer> getIntegers() {
        return integers;
    }

    public double averageOfIntegers() {
        double average = 0.0;
        for(int i=0; i<10; i++){
            integers.add(new Random().nextInt(10));
        }


        for(int number: integers){
            average+= number;
        }
        return average/(integers.size());
    }
}
