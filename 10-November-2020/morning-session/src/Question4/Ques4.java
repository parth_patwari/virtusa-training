package Question4;


import java.util.*;

public class Ques4 {
    private List<String> stringList = new ArrayList<>();

    public List<String> getStringList() {
        return stringList;
    }

    public Ques4(){
        this.generateList();
    }
    private void generateList(){
       
        this.stringList.add("Bat");
        this.stringList.add("Max");
        this.stringList.add("John");
        this.stringList.add("Jake");
        this.stringList.add("Josh");
        this.stringList.add("Henry");
        this.stringList.add("Murry");
        this.stringList.add("David");
        this.stringList.add("Nishta");
    }
    private Map<Integer, List<String>> generateMap(){
        Map<Integer, List<String>> map = new HashMap<>();
        for(String s:this.stringList){
            if (!map.containsKey(s.length())) {
                map.put(s.length(), new ArrayList<>());

            }
            map.get(s.length()).add(s);
        }
        return map;
    }
    public void displayMap(){
        for(Map.Entry<Integer, List<String>> entry: this.generateMap().entrySet()){
            System.out.println(entry.getKey()+ ": " + entry.getValue());
        }
    }

}
