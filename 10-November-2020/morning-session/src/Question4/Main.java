package Question4;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Ques4 ques4 = new Ques4();
        ques4.displayMap();

        List<String> list = ques4.getStringList();
        Map<Integer, List<String>> map = list.stream().collect(Collectors.groupingBy(String::length));

        System.out.println(map);
    }
}
