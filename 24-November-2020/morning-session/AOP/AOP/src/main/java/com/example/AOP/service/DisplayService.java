package com.example.AOP.service;

import com.example.AOP.business.DisplayData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DisplayService {


    public void print(){
        System.out.println("This is the print method.......");
    }
}
