package com.example.AOP.controller;


import com.example.AOP.service.DisplayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aop")
public class BaseController {

    @Autowired
    DisplayService displayService;

    @GetMapping("/test")
    public void controller_print(){
        displayService.print();
    }
}
