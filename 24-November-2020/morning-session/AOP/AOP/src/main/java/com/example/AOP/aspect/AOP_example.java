package com.example.AOP.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Aspect
@Configuration
public class AOP_example {

    @Before("execution(public void print())")
    public void before_join(){
        System.out.println("Before print() function.....");
    }

    @After("execution(public void print())")
    public void after_join(){
        System.out.println("After print() function.....");
    }

    @Around("execution(public void print())")
    public void around_join(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("Around Join point before print()");
        joinPoint.proceed();
        System.out.println("Around Join point after print()");
    }
}
